﻿using eWolf.PipeBuilder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using eWolf.PipeBuilder.Helpers;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class CreatePipesFromScript : MonoBehaviour
{
    // Link to an pipe builder base so we will have all the setting we need.
    public PipeBase Pipe;

    private List<PipeNode> _pipes = new List<PipeNode>();

    private List<Vector3> _referencePositions = new List<Vector3>();

    private static CreatePipesFromScript _instance = null;

    public static CreatePipesFromScript Instance
    {
        get
        {
            if (_instance == null)
            {
                var pipesCreators = FindObjectsOfType<CreatePipesFromScript>();
                if (pipesCreators.Any())
                {
                    _instance = pipesCreators[0];
                }
                else
                {
                    Debug.LogError($"No instance of {nameof(CreatePipesFromScript)} exists in the scene.");
                }
            }

            return _instance;
        }
    }

    public void RebuildPipes()
    {
        Debug.Log($"There is {GpsUpdater.Instance.Points.Count} points. Trying to REBUILD pipes");
        ClearAllPipes();
        
        bool first = true;
        PipeNode currentPipeNode = null;
        
        var cylinders = GpsUpdater.Instance.Points.Select(it => it.GameObject).ToList();
        
        foreach (GameObject cylinder in cylinders)
        {
            if (first)
            {
                currentPipeNode = Pipe.AddPipes().GetComponent<PipeNode>();
                first = false;
            }
            else
            {
                currentPipeNode = currentPipeNode.ExtendPipe().GetComponent<PipeNode>();
            }
            var pos = cylinder.transform.position;
            
            currentPipeNode.transform.position = pos;
            _pipes.Add(currentPipeNode);
            _referencePositions.Add(new Vector3(pos.x, pos.y, pos.z));
        }
        Pipe.BuildPipes();
    }

    public void RefreshPipes()
    {
        Debug.Log($"There is {GpsUpdater.Instance.Points.Count} points. Trying to build pipes");
        var cylinders = GpsUpdater.Instance.Points.Select(it => it.GameObject).ToList();
        for (var i = 0; i < cylinders.Count; i++)
        {
            if (cylinders.Count == 1) break;
            
            var cylinder = cylinders[i];
            var pos = cylinder.transform.position;
    
            var referenceTransform = _referencePositions[i];
    
            if (Vector3.Distance(pos, referenceTransform) > 0)
            {
                _pipes[i].transform.position = cylinder.transform.position;
                _referencePositions[i] = new Vector3(pos.x, pos.y, pos.z);
                Pipe.BuildPipes();
            }
        }
    }

    private void ClearAllPipes()
    {
        _pipes = new List<PipeNode>();
        _referencePositions = new List<Vector3>();
        foreach (Transform child in Pipe.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
}
