using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using TMPro;

using UnityEngine;

public class DescriptionEditor : MonoBehaviour
{
    public GameObject ObjectToEdit = null;

    private static DescriptionEditor _instance = null;

    public static DescriptionEditor Instance
    {
        get
        {
            if (_instance == null)
            {
                var descriptionEditors = FindObjectsOfType<DescriptionEditor>();
                if (descriptionEditors.Any())
                {
                    _instance = descriptionEditors[0];
                }
                else
                {
                    Debug.LogError($"No instance of {nameof(DescriptionEditor)} exists in the scene.");
                }
            }

            return _instance;
        }
    }

    public void SaveDescription()
    {
        if (ObjectToEdit == null)
        {
            throw new ArgumentNullException(nameof(ObjectToEdit));
        }
        var description = gameObject.GetComponent<TMP_InputField>().text;

        var point = GpsUpdater.Instance.GetPointByGameObject(ObjectToEdit);
        point.Description = description;

        GpsUpdater.Instance.UpdatePointDescription(point.Id, description);
        gameObject.SetActive(false);
    }
}
