using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.AR;

public class ObjctSelectorMenu : MonoBehaviour
{
    [SerializeField] private ARPlacementInteractable _placementInteractable;
    [SerializeField] private GameObject _cylinderPrefab;
    [SerializeField] private GameObject _cornerPrefab;

    public void SelectCylinder()
    {
        _placementInteractable.placementPrefab = _cylinderPrefab;
    }

    public void SelectCorner()
    {
        _placementInteractable.placementPrefab = _cornerPrefab;
    }
}
