using System;
using System.Collections.Generic;
using System.Linq;

using ARLocation;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.AR;

public class CylinderPlacementManager : MonoBehaviour
{
    [SerializeField] private string _namePrefix;
    [SerializeField] private string _meshId;
    [SerializeField] private string _description;
    [SerializeField] private GameObject _descriptionGameObject;

    public void OnCylinderCreated(ARObjectPlacementEventArgs args)
    {
        Debug.Log("ON CREATED");
        if (args is null)
        {
            throw new ArgumentNullException(nameof(args));
        }

        if (args.placementObject is null)
        {
            throw new ArgumentNullException(nameof(args.placementObject));
        }

        var gpsUpdater = GpsUpdater.Instance;

        var location = ARLocationProvider.Instance.GetLocationForWorldPosition(args.placementObject.transform.position);

        var point = new Point
        {
            Id = gpsUpdater.Points.Count,
            GameObject = args.placementObject,
            Name = _namePrefix + gpsUpdater.Points.Count,
            MeshId = _meshId,
            Description = _description,
            Altitude = Convert.ToInt32(location.Altitude),
            Location = new List<double> { location.Latitude, location.Longitude }
        };

        GpsUpdater.Instance.AddPoint(point);
    }

    public void OnFirstHoverEntered(HoverEnterEventArgs args)
    {
        if (args is null)
        {
            throw new ArgumentNullException(nameof(args));
        }
        
        Debug.Log("FIRST HOVER ENTERED");
    }
    
    public void OnLastHoverExited(HoverExitEventArgs args)
    {
        if (args is null)
        {
            throw new ArgumentNullException(nameof(args));
        }
        
        CreatePipesFromScript.Instance.RefreshPipes();
        
        Debug.Log("LAST HOVER EXITED");
    }

    public void OnHoverEntered([NotNull] HoverEnterEventArgs args)
    {
        if (args is null)
        {
            throw new ArgumentNullException(nameof(args));
        }
        
        Debug.Log("HOVER ENTERED");
    }
    
    public void OnHoverExited([NotNull] HoverExitEventArgs args)
    {
        if (args is null)
        {
            throw new ArgumentNullException(nameof(args));
        }
        
        CreatePipesFromScript.Instance.RefreshPipes();
        
        Debug.Log("HOVER EXITED");
    }

    public void OnCylinderSelected(SelectEnterEventArgs args)
    {
        if (args is null)
        {
            throw new ArgumentNullException(nameof(args));
        }
        
        Debug.Log("SELECTED");

        //TODO: open description box
        _descriptionGameObject.SetActive(true);
    }

    public void OnCylinderDeselected(SelectExitEventArgs args)
    {
        if (args is null)
        {
            throw new ArgumentNullException(nameof(args));
        }
        
        Debug.Log("DESELECTED");

        // var pointToUpdate = GpsUpdater.Instance.Points.FirstOrDefault(it => it.GameObject == args.interactable.gameObject);
        //
        // if (pointToUpdate != null)
        // {
        //     pointToUpdate.Description = "Some description";
        // }
        
        //TODO: close description box
        _descriptionGameObject.SetActive(false);
        CreatePipesFromScript.Instance.RebuildPipes();
    }

    public void OnActivated(ActivateEventArgs args)
    {
        if (args is null)
        {
            throw new ArgumentNullException(nameof(args));
        }
        
        Debug.Log("ACTIVATED");
    }

    public void OnDeactivated(DeactivateEventArgs args)
    {
        if (args is null)
        {
            throw new ArgumentNullException(nameof(args));
        }
        
        Debug.Log("DEACTIVATED");
    }
}
