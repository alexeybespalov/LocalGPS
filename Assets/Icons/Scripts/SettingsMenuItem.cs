using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenuItem : MonoBehaviour
{
    [HideInInspector] public Image img;
    [HideInInspector] public Transform trans;

    SettingsMenu settingsMenu;
    Button button;
    Toggle toggle;
    int index;

    void Awake()
    {
        img = GetComponent<Image>();
        trans = transform;

        settingsMenu = trans.parent.GetComponent<SettingsMenu>();

        //-1 to ignore the main button
        index = trans.GetSiblingIndex() - 1;

        //add click listener
        button = GetComponent<Button>();
        if (button != null)
        {
            button.onClick.AddListener(OnItemClick);
            return;
        }

        toggle = GetComponent<Toggle>();
        if (toggle != null)
        {
            toggle.onValueChanged.AddListener((b) => OnItemClick());
        }
    }

    void OnItemClick()
    {
        settingsMenu.OnItemClick(index);
    }

    void OnDestroy()
    {
        if (button != null)
        {
            //remove click listener to avoid memory leaks
            button.onClick.RemoveListener(OnItemClick);
        }

        if (toggle != null)
        {
            toggle.onValueChanged.RemoveListener((b) => OnItemClick());
        }
    }
}
