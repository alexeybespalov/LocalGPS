using System;
using System.Collections.Generic;
using System.Linq;

using JetBrains.Annotations;

using UnityEngine;

namespace ARLocation
{
    public class WebMapLoader : MonoBehaviour
    {
        /// <summary>
        ///   The `PrefabDatabase` ScriptableObject, containing a dictionary of Prefabs with a string ID.
        /// </summary>
        public PrefabDatabase PrefabDatabase;

        /// <summary>
        ///   If true, enable DebugMode on the `PlaceAtLocation` generated instances.
        /// </summary>
        public bool DebugMode;

        private List<GameObject> _stages = new List<GameObject>();
        private List<PlaceAtLocation> _placeAtComponents = new List<PlaceAtLocation>();

        private static WebMapLoader _instance = null;

        public static WebMapLoader Instance
        {
            get
            {
                if (_instance == null)
                {
                    var webMapLoaders = FindObjectsOfType<WebMapLoader>();
                    if (webMapLoaders.Any())
                    {
                        _instance = webMapLoaders[0];
                    }
                    else
                    {
                        Debug.LogError($"No instance of {nameof(WebMapLoader)} exists in the scene.");
                    }
                }

                return _instance;
            }
        }

        public List<GameObject> BuildGameObjects([NotNull] List<Point> points)
        {
            if (points == null)
            {
                throw new ArgumentNullException(nameof(points));
            }

            if (points.Count == 0)
            {
                throw new ArgumentException("Value cannot be an empty collection.", nameof(points));
            }
            
            var createdInstances = new List<GameObject>();

            foreach (var entry in points)
            {
                var prefab = PrefabDatabase.GetEntryById(entry.MeshId);

                if (!prefab)
                {
                    Debug.LogWarning($"[ARLocation#WebMapLoader]: Prefab {entry.MeshId} not found.");
                    continue;
                }

                var placementOptions = new PlaceAtLocation.PlaceAtOptions()
                    {
                        MovementSmoothing = entry.MovementSmoothing,
                        MaxNumberOfLocationUpdates = entry.MaxNumberOfLocationUpdates,
                        UseMovingAverage = entry.UseMovingAverage,
                        HideObjectUntilItIsPlaced = entry.HideObjectUtilItIsPlaced
                    };

                var location = new Location()
                    {
                        Latitude = entry.InnerLocation.Latitude,
                        Longitude = entry.InnerLocation.Longitude,
                        Altitude = entry.Altitude,
                        AltitudeMode = entry.GetAltitudeMode(),
                        Label = entry.Name
                    };

                var instance = PlaceAtLocation.CreatePlacedInstance(prefab,
                                                                    location,
                                                                    placementOptions,
                                                                    DebugMode);

                createdInstances.Add(instance);
                //GetComponent<LineDrawer>().SetCylinders(createdInstances);
            }

            _stages = createdInstances;

            return _stages;
        }
    }
}
