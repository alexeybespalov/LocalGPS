using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class JsonFileManager : MonoBehaviour
{
    private static JsonFileManager _instance = null;

    private string _basePath = "/storage/emulated/0/LocalGPS/";

    public static JsonFileManager Instance
    {
        get
        {
            if (_instance == null)
            {
                var fileManagers = FindObjectsOfType<JsonFileManager>();
                if (fileManagers.Any())
                {
                    _instance = fileManagers[0];
                }
                else
                {
                    Debug.LogError("No instance of JsonFileManager exists in the scene.");
                }
            }

            return _instance;
        }
    }

    private void Start()
    {
        if (!Directory.Exists(_basePath))
        {
            Directory.CreateDirectory(_basePath);
        }
    }

    public string LoadFile(string fileName)
    {
        return File.ReadAllText(_basePath + fileName + ".json");
    }

    public void SaveFile(string fileName)
    {
        var serializeSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        string json = JsonConvert.SerializeObject(GpsUpdater.Instance.Points, Formatting.None, serializeSettings);

        string savePath = _basePath + fileName + ".json";

        File.WriteAllText(savePath, json);
    }

    public List<string> GetJsonFileNames()
    {
        return Directory.GetFiles(_basePath, "*.json")
            .Select(Path.GetFileNameWithoutExtension)
            .ToList();
    }
}
