
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using ARLocation;

using TMPro;

using UnityEngine;

public class LocationDisplay : MonoBehaviour
{
    [SerializeField] private GameObject _sceneRoot;

    [SerializeField] private TextMeshProUGUI _latitude;
    [SerializeField] private TextMeshProUGUI _longitude;
    
    void FixedUpdate()
    {
        ARLocationProvider.Instance.OnLocationUpdatedEvent(LocationUpdatedHandler, true);
        
        //var location = ARLocationProvider.Instance.GetLocationForWorldPosition(_sceneRoot.transform.position);
    }
    
    private void LocationUpdatedHandler(LocationReading currentLocation, LocationReading lastLocation)
    {
        _latitude.SetText(currentLocation.latitude.ToString(CultureInfo.InvariantCulture));
        _longitude.SetText(currentLocation.longitude.ToString(CultureInfo.InvariantCulture));
    }
}
