
using System;

using TMPro;

using UnityEngine;

public class SaveFileButtonScript : MonoBehaviour
{
    [SerializeField] private TMP_InputField _inputField;

    public void OnSaveButtonClick()
    {
        if (string.IsNullOrEmpty(_inputField.text))
        {
            throw new InvalidOperationException("File name must contain value");
        }

        JsonFileManager.Instance.SaveFile(_inputField.text);
    }
}
