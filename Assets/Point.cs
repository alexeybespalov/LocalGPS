using System.Collections.Generic;

using ARLocation;

using UnityEngine;

public class Point
{
    public int Id { get; set; }

    public GameObject GameObject { get; set; }

    public int Altitude { get; set; } = 0;

    public string AltitudeMode { get; set; } = "GroundRelative";

    public string Name { get; set; }

    public string MeshId { get; set; }

    public float MovementSmoothing { get; set; } = 0.05f;

    public int MaxNumberOfLocationUpdates { get; set; } = 5;

    public bool UseMovingAverage { get; set; }

    public bool HideObjectUtilItIsPlaced { get; set; } = true;

    public string Description { get; set; }

    public List<double> Location { get; set; }

    public Location InnerLocation 
    {
        get
        {
            return new Location
            {
                Latitude = Location[0],
                Longitude = Location[1],
            };
        }
        set
        {
            Location[0] = value.Latitude;
            Location[1] = value.Longitude;
        }
    }

    public AltitudeMode GetAltitudeMode()
    {
        if (AltitudeMode == "GroundRelative")
        {
            return ARLocation.AltitudeMode.GroundRelative;
        }
        else if (AltitudeMode == "DeviceRelative")
        {
            return ARLocation.AltitudeMode.DeviceRelative;
        }
        else if (AltitudeMode == "Absolute")
        {
            return ARLocation.AltitudeMode.Absolute;
        }
        else
        {
            return ARLocation.AltitudeMode.Ignore;
        }
    }
}

public class Location
{
    public double Latitude { get; set; }

    public double Longitude { get; set; }
}
