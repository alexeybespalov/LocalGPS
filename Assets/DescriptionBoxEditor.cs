using UnityEngine;
using UnityEngine.InputSystem;

public class DescriptionBoxEditor : MonoBehaviour
{
    private InputActions touchControls;

    private void Awake()
    {
        touchControls = new InputActions();
    }

    private void Start()
    {
        touchControls.Touch.TouchPress.started += ctx => StartTouch(ctx);
        touchControls.Touch.TouchPress.canceled += ctx => EndTouch(ctx);
    }

    private void StartTouch(InputAction.CallbackContext context)
    {
        Debug.Log("touched:" + touchControls.Touch.TouchPosition.ReadValue<Vector2>());
    }

    private void EndTouch(InputAction.CallbackContext context)
    {
        RaycastHit hitInfo = new RaycastHit();
        if (Physics.Raycast(Camera.main.ScreenPointToRay(touchControls.Touch.TouchPosition.ReadValue<Vector2>()), out hitInfo))
        {
            var parent = GameObject.FindGameObjectWithTag("DescriptionEditor");
            var child = parent.transform.GetChild(0).gameObject;
            child.SetActive(true);
            var descriptionEditor = child.GetComponent<DescriptionEditor>();
            descriptionEditor.ObjectToEdit = transform.parent.gameObject;
        }
        else
        {
            Debug.Log("No hit");
        }
        Debug.Log("Touch ended: " + touchControls.Touch.TouchPosition.ReadValue<Vector2>());
    }

    private void OnEnable()
    {
        touchControls.Enable();
    }

    private void OnDisable()
    {
        touchControls.Disable();
    }

    //void Update()
    //{
    //    if (Touchscreen.current?.position != null)
    //    {
    //        RaycastHit hitInfo = new RaycastHit();
    //        if (Physics.Raycast(Camera.main.ScreenPointToRay(Touchscreen.current.position.ReadValue()), out hitInfo))
    //        {
    //            var descriptionText = GameObject.FindGameObjectWithTag("DescriptionEditor");
    //            var descriptionEditor = descriptionText.GetComponent<DescriptionEditor>();
    //            descriptionEditor.ObjectToEdit = transform.parent.gameObject;
    //            descriptionText.SetActive(true);
    //        }
    //        else
    //        {
    //            Debug.Log("No hit");
    //        }
    //    }
    //}
}
