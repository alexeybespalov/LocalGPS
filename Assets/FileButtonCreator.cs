using System.Linq;

using TMPro;

using UnityEngine;
using UnityEngine.UI;

public class FileButtonCreator : MonoBehaviour
{
    [SerializeField] private GameObject _loadMenuRoot;
    [SerializeField] private GameObject _buttonPrefab;
    [SerializeField] private GameObject _menuRoot;
    [SerializeField] private TextMeshProUGUI _fileTitle;

    public void UpdateButtonsFromFiles()
    {
        RemoveAllButtonsFromParentObject();
        InstantiateButtonsFromFileList();
    }

    private void InstantiateButtonsFromFileList()
    {
        var fileNames = JsonFileManager.Instance.GetJsonFileNames();
        //var fileNames = new List<string>();
        //fileNames.Add("myFile");
        foreach (var fileName in fileNames)
        {
            var newButton = GetButtonFromPrefab(fileName);
            newButton.transform.parent = _loadMenuRoot.transform;
            newButton.transform.position = new Vector3(0, 0, 0);
        }
    }

    private GameObject GetButtonFromPrefab(string fileName)
    {
        var newButton = Instantiate(_buttonPrefab);

        var rootButton = newButton.GetComponent<Button>();
        rootButton.onClick.AddListener(() => OnButtonClick(fileName));

        var innerButtons = newButton.GetComponentsInChildren<Button>();
        foreach (var button in innerButtons)
        {
            button.onClick.AddListener(() => OnButtonClick(fileName));
        }

        var buttonText = newButton.GetComponentInChildren<TextMeshProUGUI>();
        buttonText.SetText(fileName);

        return newButton;
    }

    private void RemoveAllButtonsFromParentObject()
    {
        var existingButtons = _loadMenuRoot
            .GetComponentsInChildren<Transform>()
            .Where(it => it.CompareTag(_buttonPrefab.tag)).ToList();

        foreach (var button in existingButtons)
        {
            Destroy(button.gameObject);
        }
    }

    private void OnButtonClick(string fileName)
    {
        _menuRoot.SetActive(false);
        GpsUpdater.Instance.UpdatePointsFromJson(JsonFileManager.Instance.LoadFile(fileName));
        _fileTitle.gameObject.SetActive(true);
        _fileTitle.SetText(fileName);
    }
}
