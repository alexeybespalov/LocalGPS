﻿using System;
using System.Collections.Generic;
using System.Linq;

using ARLocation;

using Newtonsoft.Json;

using TMPro;

using UnityEngine;

public class GpsUpdater : MonoBehaviour
{    
    [SerializeField] public List<Point> Points = new List<Point>();

    private static GpsUpdater _instance = null;

    public static GpsUpdater Instance
    {
        get
        {
            if (_instance == null)
            {
                var gpsUpdaters = FindObjectsOfType<GpsUpdater>();
                if (gpsUpdaters.Any())
                {
                    _instance = gpsUpdaters[0];
                }
                else
                {
                    Debug.LogError("No instance of GpsUpdater exists in the scene.");
                }
            }

            return _instance;
        }
    }

    public Point GetPointByGameObject(GameObject gameObject)
    {
        if (gameObject == null)
        {
            throw new ArgumentNullException(nameof(gameObject));
        }

        var point = Points.FirstOrDefault(p => p.GameObject == gameObject);

        if (point == null)
        {
            Debug.LogError($"There is no such an object!!! Total count of objects in List is:{Points.Count}");
        }

        return point;
    }

    public void UpdatePointsFromJson(string pointsJson)
    {
        if (pointsJson == null)
        {
            throw new ArgumentNullException(nameof(pointsJson));
        }

        DestroyAllPointObjects();

        Points = JsonConvert.DeserializeObject<List<Point>>(pointsJson);

        for (int i = 0; i < Points.Count; i++)
        {
            Points[i].Id = i;
        }

        var createdInstances = WebMapLoader.Instance.BuildGameObjects(Points ?? new List<Point>());

        for (int i = 0; i < createdInstances.Count; i++)
        {
            Points[i].GameObject = createdInstances[i];
        }
    }

    public void AddPoint(Point point)
    {
        if (point == null)
        {
            throw new ArgumentNullException(nameof(point));
        }

        if (point.GameObject == null)
        {
            throw new ArgumentNullException(nameof(point.GameObject));
        }

        Points.Add(point);
        CreatePipesFromScript.Instance.RebuildPipes();
    }

    public void UpdatePointDescription(int id, string description)
    {
        foreach(var point in Points)
        {
            if (point.Id == id)
            {
                point.Description = description;
                var text = point.GameObject.GetComponentInChildren<TextMeshPro>(includeInactive: true);

                if (text == null)
                {
                    Debug.Log("TEXT IS NULL");
                }
                text.text = description;
            }
        }
        
    }

    public void DestroyAllPointObjects()
    {
        foreach (var point in Points)
        {
            Destroy(point.GameObject);
        }

        Points.Clear();
    }
}
